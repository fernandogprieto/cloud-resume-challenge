  // Check if the visitor count is already stored in local storage
  if (localStorage.getItem("visitorCount")) {
    // If it is, retrieve the count and increase it by 1
    var visitorCount = parseInt(localStorage.getItem("visitorCount"));
    visitorCount++;
  } else {
    // If it isn't, set the visitor count to 1
    var visitorCount = 1;
  }

  // Store the updated visitor count in local storage
  localStorage.setItem("visitorCount", visitorCount);

  // Update the visitor count on the page
  document.getElementById("visitor-counter").innerHTML = "You are visitor number " + visitorCount + " of my Cloud Resume Challenge";